
const mainReducer = (state: any, action: any) => {
  switch (action.type) {
    case "INPUT_CHANGED":
        return {...state, inputValue: action.payload.value};
    default:
      return state
  }
}

export default mainReducer