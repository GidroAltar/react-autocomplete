import * as React from "react"
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject, ActionCreator, Action } from "redux"

import "./autocomplete.css"

interface IState {
    inputValue: any
}

interface IProps {
    suggestions: Array<string>,
    inputOnChangeHandle: ActionCreator<Action>,
    inputValue: string
}

class Autocomplete extends React.Component<IProps, IState> {
    suggestionsList: HTMLUListElement = null;

    renderSuggestions = (suggestions: Array<string>) => {
        return <ul className = "list-group suggestions-block" ref={(list) => { this.suggestionsList = list; }}>
                    {
                        suggestions.map((suggestion, index) => {
                            return <li key = { index }  onMouseDown = { this.handleSuggestionClick } className="suggestion list-group-item">{ suggestion }</li>
                        })
                    }
                </ul>
    }

    getSuggestions = (value: string): Array<string> => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        const { suggestions } = this.props;

        return inputLength === 0 ? [] : suggestions.filter(suggestion =>
            suggestion.toLowerCase().slice(0, inputLength) === inputValue
        );
    }

    handleInputChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.suggestionsList.classList.remove("invisible");
        this.props.inputOnChangeHandle(e.currentTarget.value);
    }

    handleSuggestionClick = (e: React.MouseEvent<HTMLElement>) => {
        this.props.inputOnChangeHandle(e.currentTarget.innerHTML);
    }

    handleInputOnBlur = () => {
        this.suggestionsList.classList.add("invisible");
    }

    render() {
        const { inputValue } = this.props;

        return <div className = "suggestions-block-container">
            <input type = "text" className = "form-control" value = { inputValue } onChange = {this.handleInputChange} onBlur = { this.handleInputOnBlur }
             onFocus = {this.handleInputChange}></input>
            {
                this.renderSuggestions(this.getSuggestions(inputValue))
            }
        </div>
    }
}

export default Autocomplete;