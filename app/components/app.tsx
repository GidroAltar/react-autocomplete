import * as React from "react"
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux"

import Autocomplete  from "./autocomplete"
import { appActionsMap } from "../actions/index"

interface IState {
    
}

interface IProps {
    appState: any,
    appActions: any
}

const testSuggestions = [
    "C#", "C", "C++", "Java", "JavaScript", "Python","Ruby", "TypeScript", "F#"
]

const testSuggestions2 = [
    "Git", "Databases", "HTML", "CSS", "REST", "React", "Redux"
]

class App extends React.Component<IProps, IState> {

    constructor(props: any) {
        super(props);
    }
    
    render() {
        const { inputOnChangeHandle } = this.props.appActions;
        const { inputValue } = this.props.appState;
        return <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <form>
                        <label>Языки программирования:</label>
                        <Autocomplete 
                            suggestions = { testSuggestions }
                            inputOnChangeHandle = { inputOnChangeHandle }
                            inputValue = { inputValue }
                        />
                    </form>
                </div>
            </div>
        </div>
    }
}

function mapStateToProps(state: any) {
    return {
        appState: state
    }
}

function mapDispatchToProps(dispatch: any) {
    return {
        appActions: bindActionCreators(appActionsMap, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);