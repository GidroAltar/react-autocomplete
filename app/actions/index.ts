import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";

export const inputChanged: ActionCreator<Action> = (value: string) => ({
    type: "INPUT_CHANGED",
    payload: {
      value: value
    }
  })

export const appActionsMap: ActionCreatorsMapObject = {
    inputOnChangeHandle: inputChanged
}

export default appActionsMap;